//
//  ReferenceView.swift
//  emapp
//
//  Created by Hans Cappelle on 21/03/2021.
//

import SwiftUI
import WebKit

// simple webview with including loading indication
struct ReferenceView: UIViewControllerRepresentable {
    
    let url: URL
    var webviewController: WebviewController = WebviewController()

    func makeUIViewController(context: Context) -> WebviewController {
        let request = URLRequest(url: self.url, cachePolicy: .returnCacheDataElseLoad)
        webviewController.webview.load(request)
        webviewController.webview.allowsBackForwardNavigationGestures = true
        return webviewController
    }

    func updateUIViewController(_ webviewController: WebviewController, context: Context) {
        let request = URLRequest(url: self.url, cachePolicy: .returnCacheDataElseLoad)
        webviewController.webview.load(request)
    }
    
    func reload(){
        let request = URLRequest(url: self.url, cachePolicy: .returnCacheDataElseLoad)
        webviewController.webview.load(request)
    }
    
}

class WebviewController: UIViewController {
    
    lazy var webview: WKWebView = WKWebView()
    lazy var progressbar: UIProgressView = UIProgressView()

    override func viewDidLoad() {
        super.viewDidLoad()

        webview.frame = self.view.frame
        view.addSubview(self.webview)
        // prevent webview to hide after tabs of ContentView, tabbar bottom height is 64
        webview.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints([
            webview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            webview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            webview.topAnchor.constraint(equalTo: self.view.topAnchor),
            webview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])

        self.view.addSubview(self.progressbar)
        self.progressbar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addConstraints([
            self.progressbar.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.progressbar.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.progressbar.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
        ])

        self.progressbar.progress = 0.1
        webview.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        // these will trigger the onPause and onResume methods
        NotificationCenter.default.addObserver(self, selector: #selector(onResume), name:
            UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc func onResume() {
        print("view resumed so refreshing data")
    }

    // MARK: - Web view progress
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        switch keyPath {
        case "estimatedProgress":
            if self.webview.estimatedProgress >= 1.0 {
                UIView.animate(withDuration: 0.3, animations: { () in
                    self.progressbar.alpha = 0.0
                }, completion: { finished in
                    self.progressbar.setProgress(0.0, animated: false)
                })
            } else {
                self.progressbar.isHidden = false
                self.progressbar.alpha = 1.0
                progressbar.setProgress(Float(self.webview.estimatedProgress), animated: true)
            }
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
}

#if DEBUG
struct WebView_Previews : PreviewProvider {
    static var previews: some View {
        ReferenceView(url: URL(string: "https://www.apple.com")!)
    }
}
#endif
