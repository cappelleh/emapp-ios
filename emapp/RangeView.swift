//
//  RangeView.swift
//  emapp
//
//  Created by Hans Cappelle on 21/03/2021.
//

import SwiftUI

struct RangeView: View {
    
    // FIXME move all logic to ObservableObject
    
    @State private var selectedCapacity = 14.4
    let capacities = [7.2, 13.4, 14.4, 15.5, 18.0, 21.5] // battery capacities in doubles
    let nominal = [6.3, 11.7, 12.6, 13.6, 15.8, 18.9] // npminal battery capacities in doubles
    //let brands = ["Zero FX", "Energica", "Zero", "HD LiveWire", "Zero +PT", "Energica +"]
    @State private var selectedSoc: Double = 85
    
    let roadTypes = ["City", "City/Road", "City/Highway", "Road", "Highway"]
    let range = [19.93, 15, 13.4, 12.08, 9.93] // full battery range per kWh
    @State private var selectedRoadType = "Road"
    
    let ridingStyles = ["Conservative", "Aggressive"]
    let ridingStylePenalties = [1.0, 0.9]
    @State private var selectedRidingStyle = "Conservative"
    
    let landscapes = ["Flat", "Hills"]
    let landscapePenalties = [1.0, 0.9]
    @State private var selectedLandscape = "Flat"

    let weatherConditions = ["Cold", "Mild", "Warm"]
    let weatherPenalties = [0.9, 0.95, 1.0]
    @State private var selectedWeather = "Mild"
    
    // charge time calculation
    @State private var selectedCharger = 3
    let chargers = [3, 6, 9, 12, 20, 24]
    
    func calculatedRange() -> Double {
        let soc = selectedSoc / 100 // soc percentage
        let roadIdx = roadTypes.firstIndex(of: selectedRoadType)! // selected road index
        let ridingStyleIdx = ridingStyles.firstIndex(of: selectedRidingStyle) ?? 0
        let landscapeIdx = landscapes.firstIndex(of: selectedLandscape) ?? 0
        let weatherIdx = weatherConditions.firstIndex(of: selectedWeather) ?? 0
        // given range for road minus penalties
        return selectedCapacity * range[roadIdx] * soc
            * ridingStylePenalties[ridingStyleIdx]
            * landscapePenalties[landscapeIdx]
            * weatherPenalties[weatherIdx] * energicaCorrection()
    }
    
    private func energicaCorrection() -> Double {
        return selectedCapacity == 13.4 || selectedCapacity == 21.5 ? 0.9 : 1.0
    }
    
    func calculateChargeTime() -> Double {
        let soc = selectedSoc / 100
        let nominalCapacity = nominal[capacities.firstIndex(of: selectedCapacity)!]
        let amountToChargeInkWh = nominalCapacity - soc * nominalCapacity
        // either from full capacity * 0.85 or from nominal capacity
        return (amountToChargeInkWh / Double(selectedCharger)) * 60 // in minutes
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func formattedChargeTime() -> String {
        let calculated = calculateChargeTime() * 60
        let (h, m, _) = secondsToHoursMinutesSeconds(seconds: Int(calculated))
        return ("\(h)h \(m)m")//, \(s)s")
    }
    
    var body: some View {
        
        // TODO check smaller screen support
        VStack{
            VStack{
                Text("E.M.App - Electric Motorcycle App").bold().padding()
                Text("Select battery capacity first. Then pick State of Charge (SOC) and riding conditions.")
            }
            VStack{
                HStack{
                    Picker("Battery", selection: $selectedCapacity){
                        ForEach(capacities, id: \.self) {Text("\($0, specifier: "%.1f")")}
                }.pickerStyle(SegmentedPickerStyle())
                    Text("kWh")
                }
                Text("Selected Battery: \(selectedCapacity, specifier: "%.1f") kWh (\(nominal[capacities.firstIndex(of: selectedCapacity)!], specifier: "%.1f"))").bold()
                HStack{
                    Text("SOC")
                    Slider(value: $selectedSoc, in: 0...100)
                    Text("\(Int(selectedSoc))%")
                }
                Text("\(Int(calculatedRange())) km or \(Int(calculatedRange() * 0.621371192)) mi").font(.largeTitle).foregroundColor(Color.blue)
                //Text("Road Type (speed)")
                Picker("", selection: $selectedRoadType) {
                    ForEach(roadTypes, id: \.self) {Text($0)}
                }.pickerStyle(SegmentedPickerStyle())
                HStack{
                    Text("Riding: ")
                    Picker("Riding", selection: $selectedRidingStyle){
                        ForEach(ridingStyles, id: \.self) {Text($0)}
                    }.pickerStyle(SegmentedPickerStyle())
                }
                HStack{
                    Text("Landscape: ")
                    Picker("Landscape", selection: $selectedLandscape){
                        ForEach(landscapes, id: \.self) {Text($0)}
                    }.pickerStyle(SegmentedPickerStyle())
                }
                HStack{
                    Text("Weather: ")
                    Picker("Weather", selection: $selectedWeather){
                        ForEach(weatherConditions, id: \.self) {Text($0)}
                    }.pickerStyle(SegmentedPickerStyle())
                }
            }
            
            VStack{
                // charge time calculation (kWh/kW = charge time in hours)
                Text("Estimated charge time: \(formattedChargeTime())").bold().padding()
                HStack{
                    Picker("Charger", selection: $selectedCharger){
                        ForEach(chargers, id: \.self) {Text($0.description)}
                }.pickerStyle(SegmentedPickerStyle())
                    Text("kW")
                }
            }
            
            // bottom spacing (fills lower section)
            Spacer()
        }.padding()
    }
}

struct RadioButton: View {
    
    let ifVariable: Bool    //the variable that determines if its checked
    let onTapToActive: ()-> Void//action when taped to activate
    let onTapToInactive: ()-> Void //action when taped to inactivate

    var body: some View {
        Group{
            if ifVariable {
                ZStack{
                    Circle()
                        .fill(Color.blue)
                        .frame(width: 20, height: 20)
                    Circle()
                        .fill(Color.white)
                        .frame(width: 8, height: 8)
                }.onTapGesture {self.onTapToInactive()}
            } else {
                Circle()
                    .fill(Color.white)
                    .frame(width: 20, height: 20)
                    .overlay(Circle().stroke(Color.gray, lineWidth: 1))
                    .onTapGesture {self.onTapToActive()}
            }
        }
    }
}

#if DEBUG
struct RangeView_Previews: PreviewProvider {
    static var previews: some View {
        RangeView()
    }
}
#endif
