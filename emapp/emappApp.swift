//
//  emappApp.swift
//  emapp
//
//  Created by Hans Cappelle on 21/03/2021.
//

import SwiftUI

@main
struct EmappApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
