//
//  ContentView.swift
//  emapp
//
//  Created by Hans Cappelle on 21/03/2021.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var bleControl = BleControl()
    @AppStorage("selectedTabIndex") var selectedTabIndex: Int = 0 // remembers selected tab
    
    var body: some View {
        
        // some app title, removed no real need for and only takes up space
        //Text("E.M.App - Electric Motorcycle App").padding()
        
        // requires a refresh so reference
        let videoView = ReferenceView(url: URL(string: "https://www.youtube.com/channel/UCMB-LNAuNJTAsFZgakBkpxQ/Videos")!)
    
        // tabs, for some tabs force refresh on loading
        TabView(selection: $selectedTabIndex) {
            MapView().tabItem { Label("Map", systemImage: "map")}.tag(1)
            Obd2ConnectView().tabItem { Label("OBDII", systemImage:"bolt.car") }.tag(2).environmentObject(bleControl)
            RangeView().tabItem { Label("Range", systemImage:"scribble") }.tag(3)
            ReferenceView(url: URL(string: "https://fotoleer.wordpress.com/electric-motorcycles/")!).tabItem { Label("Blog", systemImage: "doc.text") }.tag(4)
            videoView.tabItem { Label("Video", systemImage: "video") }.tag(5).onAppear{ videoView.reload() }
        }
        
    }
}

#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
