//
//  ObdBluetooth.swift
//  emapp
//
//  Created by Hans Cappelle on 16/01/2022.
//

import Foundation
import UIKit
import CoreBluetooth

class ObdBluetooth: NSObject {

    // The characteristic, in this case, are those for a UART communication:
    // from https://stackoverflow.com/questions/61090365/what-ble-characteristic-should-i-use-in-a-ble-ios-device-for-obdii
    //Service Service: UUID: 0000fff0-0000-1000-8000-00805f9b34fb

    //use the notify of this one...
    //  Charac.: UUID: 0000fff1-0000-1000-8000-00805f9b34fb
    //    Can read: false Can write: false  Can notify: true
    //  Charac.: UUID: 0000fff2-0000-1000-8000-00805f9b34fb

    //use the write of this one...
    //    Can read: false Can write: true  Can notify: false
      
    public static let serviceUartUUID = CBUUID.init(string: "0000fff0-0000-1000-8000-00805f9b34fb")
    public static let charxNotifyUUID = CBUUID.init(string: "0000fff1-0000-1000-8000-00805f9b34fb")
    public static let charxWriteUUID = CBUUID.init(string: "0000fff2-0000-1000-8000-00805f9b34fb")

}
