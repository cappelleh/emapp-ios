//
//  MapView.swift
//  emapp
//
//  Created by Hans Cappelle on 19/01/2022.
//

import SwiftUI
import MapKit
import CoreLocation
import SwiftyJSON

struct MapView: View {
    
    @StateObject var locationViewModel = LocationViewModel()
    
    var body: some View {
        switch locationViewModel.authorizationStatus {
        case .notDetermined:
            AnyView(RequestLocationView())
                .environmentObject(locationViewModel)
        case .restricted:
            ErrorView(errorText: "Location use is restricted.")
        case .denied:
            ErrorView(errorText: "The app does not have location permissions. Please enable them in settings.")
        case .authorizedAlways, .authorizedWhenInUse:
            TrackingView()
                .environmentObject(locationViewModel)
        default:
            Text("Unexpected status")
        }
    }
    
}

struct RequestLocationView: View {
    
    @EnvironmentObject var locationViewModel: LocationViewModel
    
    var body: some View {
        VStack {
            Image(systemName: "location.circle")
                .resizable()
                .frame(width: 100, height: 100, alignment: .center)
                .foregroundColor(.blue)
            Button(action: {
                locationViewModel.requestPermission()
            }, label: {
                Label("Allow tracking", systemImage: "location")
            })
            .padding(10)
            .foregroundColor(.white)
            .background(Color.blue)
            .clipShape(RoundedRectangle(cornerRadius: 8))
            Text("We need your permission to track you in order to display nearby chargers.")
                .foregroundColor(.gray)
                .font(.caption)
        }
    }
}

struct ErrorView: View {
    
    var errorText: String
    
    var body: some View {
        VStack {
            Image(systemName: "xmark.octagon")
                    .resizable()
                .frame(width: 100, height: 100, alignment: .center)
            Text(errorText)
        }
        .padding()
        .foregroundColor(.white)
        .background(Color.red)
    }
}

struct PlaceAnnotationView: View {
  @State private var showTitle = true
  
  let title: String
  
  var body: some View {
    VStack(spacing: 0) {
      Text(title)
        .font(.callout)
        .padding(5)
        .background(Color(.white))
        .cornerRadius(10)
        .opacity(showTitle ? 0 : 1)
      
      Image(systemName: "mappin.circle.fill")
        .font(.title)
        .foregroundColor(.red)
      
      Image(systemName: "arrowtriangle.down.fill")
        .font(.caption)
        .foregroundColor(.red)
        .offset(x: 0, y: -5)
    }
    .onTapGesture {
      withAnimation(.easeInOut) {
        showTitle.toggle()
      }
    }
  }
}

struct TrackingView: View {
    
    @EnvironmentObject var locationViewModel: LocationViewModel
    
    // map based location view
    var body: some View {
        Map(
            coordinateRegion: $locationViewModel.region,
            showsUserLocation: true,
            userTrackingMode: .constant(.follow),
            annotationItems: $locationViewModel.chargers) {
                //MapPin(coordinate: $0.wrappedValue.coordinate)
                place in MapAnnotation(coordinate: place.wrappedValue.coordinate) {
                    PlaceAnnotationView(title: place.wrappedValue.name)
                }
            }
        //.frame(width: 400, height: 300)
        //.onReceive(locationViewModel.$chargerLocations, perform: updateChargerLocations(locations:))
    }
    
    // last known location
    //var coordinate: CLLocationCoordinate2D? {
    //    locationViewModel.lastSeenLocation?.coordinate
    //}
    
    // charger locations are already set on map in annotationItems
    //private func updateChargerLocations(locations: [CLLocation]){
    //    // update locations on map here
    //    print("updating chargers on map")
    //}

}

class LocationViewModel: NSObject, ObservableObject, CLLocationManagerDelegate {
    
    @Published var authorizationStatus: CLAuthorizationStatus
    private let locationManager = CLLocationManager()
    
    @Published var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275),
        span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    
    private var chargeMapApiService = ChargeMapApiService()
    private var lastFetchedLocation: CLLocation?
    @Published var chargerLocations: [CLLocation] = []
    
    struct Charger: Identifiable {
        let id = UUID()
        let name: String
        let coordinate: CLLocationCoordinate2D
    }
    
    @Published var chargers: [Charger] = []
    
    override init() {
        
        authorizationStatus = locationManager.authorizationStatus
        super.init()
        
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()
        // For use in foreground
        //self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            // locationManager.desiredAccuracy = kCLLocationAccuracyBest
            // locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }

    }
    
    func requestPermission() {
        locationManager.requestWhenInUseAuthorization()
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authorizationStatus = manager.authorizationStatus
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //lastSeenLocation = locations.first
        // FIXED completely updating region makes the zoom level reset on each update
        // get nearby chargers if covered enough distance since last call
        fetchNearbyChargers(for: locations.first)
    }
    
    func fetchNearbyChargers(for location: CLLocation?) {
        guard let location = location else { return }
        // check if distance from last fetch is big enough to limit number of fetches
        // distance is in meters
        if( lastFetchedLocation == nil || (lastFetchedLocation?.distance(from: location))! > 10000 ){
            lastFetchedLocation = location
            // fetch
            chargeMapApiService.retrieveChargers(
                latitude: location.coordinate.latitude,
                longitude: location.coordinate.longitude,
                success: { (response) -> () in self.updateFetchedChargersOnMap(result: response) },
                failure: { (error) -> () in print("Error: \(error!.localizedDescription)") }
            )
        }
    }
    
    func updateFetchedChargersOnMap(result: JSON) {
        // store in published set of charger locations
        // TODO add more charge information to these pins (like address and start GPS route etc)
        print(result)
        chargerLocations.removeAll()
        for charger in result {
            chargerLocations.append(CLLocation(
                latitude: Double("\(charger.1["AddressInfo"]["Latitude"])") ?? 0,
                longitude: Double("\(charger.1["AddressInfo"]["Longitude"])") ?? 0
            ))
            chargers.append(Charger(
                name: "\(charger.1["AddressInfo"]["Title"]), \(charger.1["AddressInfo"]["AddressLine1"]), \(charger.1["AddressInfo"]["Postcode"])",
                coordinate: CLLocationCoordinate2D(
                    latitude: Double("\(charger.1["AddressInfo"]["Latitude"])") ?? 0,
                    longitude: Double("\(charger.1["AddressInfo"]["Longitude"])") ?? 0
                )
                )
            )
        }
    }
        
}
