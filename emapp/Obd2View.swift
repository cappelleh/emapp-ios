//
//  Obd2View.swift
//  emapp
//
//  Created by Hans Cappelle on 02/01/2022.
//

import SwiftUI
import CoreBluetooth

// Since we are using SwiftUI there is no place for UIViewControllers,
// instead create views with ObservableObjects
struct Obd2ConnectView: View {
    
    @EnvironmentObject var ble: BleControl // reference to our BLE connection
     
    var body: some View {
        VStack {
            Text("OBDII BLE Connection")
                .font(.system(.title2))
            HStack{
                Button(action: {ble.connect()}) {
                    // update text when already connected
                    if (ble.connected) {
                        Text("Disconn.")
                    }else if( ble.scanning ) {
                        Text("Stop")
                    } else {
                        Text("Connect")
                    }
                }.buttonStyle(BlueButton())
                if( ble.connected ){
                    Text("LIVE").onLongPressGesture(minimumDuration: 0.1) { ble.toggleDebugVisible()
                    }
                } else if (ble.scanning) {
                    Text("Scanning...")
                } else {
                    Text("Ready").onLongPressGesture(minimumDuration: 0.1) { ble.toggleDebugVisible()
                    }
                }
            }
            if( ble.debugVisible ){
                HStack{
                    TextField("AT commands ex: ATCRA200 001", text: $ble.atCommands)
                        .frame(width: 200, height: 44, alignment: .center)
                        .disableAutocorrection(true)
                    Button(action:{
                        ble.autoUpdate()}){Text("Auto Rep.(\(ble.monitoring ? "Y":"N"))")}
                    TextField("4", text: $ble.interval)
                        .frame(width: 25, height: 44, alignment: .center)
                        .disableAutocorrection(true)                    
                }
            }
            VStack{
                Text("State: \(ble.state) (1=IDLE, 2=AC, 16=DC)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("SOC: \(ble.soc) % (SOH: \(ble.soh)%)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("Temp: \(ble.tempMin)/\(ble.tempMax) °C (min/max)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("Voltage: \(ble.voltage, specifier: "%.1f") V (bPackV)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("Amps: \(ble.amps, specifier: "%.1f") A (bPackI)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("Power: \(ble.power, specifier: "%.1f") kW (V*I)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
                Text("Cell Balance: \(ble.cellMin)/\(ble.cellMax) mV").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
            }
            //Spacer()
            //Text("\(ble.response)")
            //    .font(.system(.body, design: .monospaced))
            HStack{
                Button(action: {
                    ble.requestUpdate()
                }){Text("Update Data")}
                    .buttonStyle(BlueButton())
                Button(action: {
                    ble.clearResponseValue()
                    ble.clearLog()
                }){Text("Clear")}
                Button(action: {
                    ble.toggleShowLogs()
                }){Text("\(ble.showLogs ? "ON":"OFF")")}
            }
            Text("Connect OBDII dongle, power on motorcycle and start scanning.")
            Spacer()
            Text("\(ble.log)").font(.system(.body, design: .monospaced)).frame(maxWidth: .infinity, alignment: .leading)
        }.padding()
    }
    
}

struct BlueButton: ButtonStyle {
    func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .padding()
            .background(Color.blue)
            .foregroundColor(.white)
            .clipShape(Capsule())
    }
}

// Controls our BLE connection, here all the logic takes place. The @Published elements is what
// can be observed within the View
class BleControl: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate, ObservableObject {
    
    // published UI values
    @Published var state = 1
    @Published var soc = 0
    @Published var soh = 0
    @Published var tempMin = 0
    @Published var tempMax = 0
    @Published var voltage = 0.0
    @Published var amps = 0.0
    @Published var power = 0.0
    @Published var cellMin = 0
    @Published var cellMax = 0
    
    @Published var connected = false
    @Published var scanning = false
    @Published var responseValue = "" // published formatted response
    @Published var log = ""
    @Published var monitoring : Bool = false
    
    @Published var debugVisible : Bool = false
    @Published var showLogs : Bool = true
    
    //@Published var atCommands : String = "201 200 203" // default CAN IDs to monitor
    @Published var atCommands : String = "ATCAF0 ATSH7DF ATCRA200 001 ATCRA201 001 ATCRA203 001"// ATCAF1 ATSH7E7 ATCRA7EF 0142 0146 015B"
    @Published var interval : String = "4"
    
    var response = "" // in app formatted response values
    var lastAtCommand = "" // last can ID will trigger visual update
    var validInterval = 4.0 // default interval in seconds between commands
    
    // implemented as a singleton
    static let BLESingleton = BleControl()
    override init() {
        // nothing required so far
    }
    
    // some constant values
    let STATE_IDLE = 1
    let STATE_AC_CHARGING = 2
    let STATE_DC_CHARGING = 16
    
    let SCAN_START = 100 // start scan range
    let SCAN_END = 899 // end scan range (rolls over at 900)
    let MONITOR_INTERVAL = 4.0 // seconds between updates in monitor mode
    
    func clearResponseValue() {
        response = ""
        responseValue = ""
    }
    
    func clearLog() {
        log = ""
    }
    
    private func log(message: String) {
        if( showLogs ){
            log = "\(message)\n\(log)" // showw on screen
        }
        print("\(Date()) \(message)") // print to debug console
        print(Date(), message, to: &Log.log) // logs to file
    }
    
    func toggleShowLogs() {
        showLogs = !showLogs
    }
                
    func toggleDebugVisible() {
        debugVisible = !debugVisible
    }
    
    private func parseTestValue() {
        response = ""
        collected = ""
        //handleReceivedValue(value: "201 10 00 00 00 00 00 00 00")
        handleReceivedValue(value: "2011000000000000000")
        handleReceivedValue(value: ">")
        handleReceivedValue(value: "  ")
        //handleReceivedValue(value: "200 0A 60 64 0B 0C E")
        handleReceivedValue(value: "2000A60640B0CE")
        handleReceivedValue(value: "3005C")
        handleReceivedValue(value: ">")
        //handleReceivedValue(value: "202 0A 60 64 0B 0C E3 FF FE")
        handleReceivedValue(value: "2020A60640B0CE3FFFE")
        handleReceivedValue(value: ">")
        //handleReceivedValue(value: "203 0F 40 43 2E 0F 36 0F 4D")
        handleReceivedValue(value: "2030F40432E0F360F4D")
        handleReceivedValue(value: ">")
    }
    
    func autoUpdate() {
        // toggles state
        if (monitoring) {
            monitoring = false
        } else {
            // parse new interval value
            validInterval = Double(interval) ?? MONITOR_INTERVAL
            monitoring = true
            monitor() // run in a loop
        }
    }
    
    func monitor(){
        if( monitoring ){
            requestUpdate() // update
            // and request a new update 10s later
            DispatchQueue.main.asyncAfter(deadline: .now() + validInterval) {
                self.monitor() // recursive
            }
        }
    }
    
    func requestUpdate() {
        // clear output first
        response = ""
        collected = ""
        #if targetEnvironment(simulator)
            parseTestValue() // also triggers fake response
        #else
            if( !connected ){
                // handle non connected state
                response = "No connection!"
                return
            }
        #endif
        // handle custom CANID requests here
        if (atCommands.count > 0) {
           // proper parsing and iteration
            let components = atCommands.components(separatedBy: " ")
            lastAtCommand = components.last ?? ""
            var initialDelay = 2.0
            for component in components {
                initialDelay = initialDelay + 1.0
                requestDelayed(atCommand: component, delay: initialDelay, show: true) {}
            }
        }
    }
    
    func requestDelayed(atCommand: String, delay: Double, show: Bool = false, next: @escaping () -> Void){
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            self.writeOutgoingValue(data: "\(atCommand)\r", logOutput: show)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                next()
            }
        }
    }
    
    func connect() {
        
        #if targetEnvironment(simulator)
            // no actual connection possible on sim
            connected = !connected
            return
        #endif
        
        // check for bt
        if( centralManager == nil ){
            centralManager = CBCentralManager(delegate: self, queue: nil)
        }
        
        if( connected ){
            // should we implement a disconnect when already connected?
            log(message: "X Closing Connection.")
            //centralManager.stopScan()
            if( peripheral != nil ){
                centralManager.cancelPeripheralConnection(peripheral)
            }
            connected = false
            scanning = false
        }
        else if( scanning ){
            // just keeps scanning, nothing found yet
            log(message: "X Scanning stopped.")
            centralManager.stopScan()
            scanning = false
            connected = false
        }
        else if( !connected && !scanning ){
            log(message: "Start scanning...")
            // Start scanning again
            scanning = true
            centralManager.scanForPeripherals(withServices: [ObdBluetooth.serviceUartUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
        }

    }

    // needed for BLE connection
    private var centralManager: CBCentralManager!
    private var peripheral: CBPeripheral!
    
    // Characteristics
    private var notifyChar: CBCharacteristic?
    private var writeChar: CBCharacteristic?
    
    // For Energica specific response parsing
    private var collected: String = ""
    private let regex = try! NSRegularExpression(pattern: "[0-9a-fA-F]+")
    private var chargeState: Int = 0

    // Handle BLE state updates
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("Central state update")
        if central.state != .poweredOn {
            scanning = false
            log(message: "X Bluetooth not enabled!")
        } else {
            scanning = true
            log(message: "Central scanning for \(ObdBluetooth.serviceUartUUID)")
            centralManager.scanForPeripherals(withServices: [ObdBluetooth.serviceUartUUID], options: [CBCentralManagerScanOptionAllowDuplicatesKey : true])
        }
    }
        
    // Handles the result of the scan
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {

        // We've found it so stop scan
        self.centralManager.stopScan()
        scanning = false

        // Copy the peripheral instance
        self.peripheral = peripheral
        self.peripheral.delegate = self

        // Connect!
        self.centralManager.connect(self.peripheral, options: nil)
    }
        
    // The handler if we do connect succesfully
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        if peripheral == self.peripheral {
            self.connected = true
            log(message: "Connected!")
            peripheral.discoverServices([ObdBluetooth.serviceUartUUID])
        }
    }
    
    // Handles discovery event
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let services = peripheral.services {
            for service in services {
                if service.uuid == ObdBluetooth.serviceUartUUID {
                    log(message: "UART Service found")
                    //Now kick off discovery of characteristics
                    peripheral.discoverCharacteristics([ObdBluetooth.charxNotifyUUID, ObdBluetooth.charxWriteUUID], for: service)
                    return
                }
            }
        }
    }

    // Handling discovery of characteristics
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        if let characteristics = service.characteristics {
            for characteristic in characteristics {
                if characteristic.uuid == ObdBluetooth.charxNotifyUUID {
                    notifyChar = characteristic
                    log(message: "Notify characteristic found")
                    
                    // enabled notify for this one
                    peripheral.setNotifyValue(true, for: notifyChar!)
                } else if characteristic.uuid == ObdBluetooth.charxWriteUUID {
                    writeChar = characteristic
                    log(message: "Write characteristic found")
                    
                    // write some outgoing data for init
                    writeOutgoingValue(data: "ATWS\r") // warm start
                    writeOutgoingValue(data: "ATE0\r") // disable echo
                    
                    writeOutgoingValue(data: "ATI\r")
                    writeOutgoingValue(data: "AT@1\r")
                    writeOutgoingValue(data: "ATSP6\r")
                    writeOutgoingValue(data: "ATAT1\r")
                    
                    writeOutgoingValue(data: "ATH1\r") // enable headers
                    writeOutgoingValue(data: "ATL0\r")
                    writeOutgoingValue(data: "ATS0\r")
                    
                    // still part of initial sequence
                    writeOutgoingValue(data: "ATCAF1\r") // initial sequence
                    writeOutgoingValue(data: "ATSH79B\r")
                    writeOutgoingValue(data: "ATFCSH79B\r")
                    writeOutgoingValue(data: "ATFCSD300000\r")
                    writeOutgoingValue(data: "ATFCSM1\r")
                    writeOutgoingValue(data: "ATSH7E7\r")
                    writeOutgoingValue(data: "ATFCSH7E7\r")
                    writeOutgoingValue(data: "ATCRA7EF\r")
                    writeOutgoingValue(data: "0902\r") // vin request
                    
                    // looped sequence
                    //writeOutgoingValue(data: "ATCAF0\r")
                    //writeOutgoingValue(data: "ATSH7DF\r")
                    //writeOutgoingValue(data: "ATCRA200\r")
                    //writeOutgoingValue(data: "001\r")
                    //writeOutgoingValue(data: "ATCAF1\r")
                    //writeOutgoingValue(data: "ATSH7E7\r")
                    //writeOutgoingValue(data: "ATCRA7EF\r")
                    //writeOutgoingValue(data: "0142\r")
                    //writeOutgoingValue(data: "0146\r")
                    //writeOutgoingValue(data: "015B\r")
                    
                    //writeOutgoingValue(data: "ATCAF0\r") // disable auto formatting
                    //writeOutgoingValue(data: "AL\r") // allow for long messages
                    //writeOutgoingValue(data: "CSM0\r") // operate in silent mode
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        // request some initial data
                        self.requestUpdate()
                    }
                }
            }
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        // disconnect
        log(message: "Disconnected")
        self.peripheral = nil
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        // receives a response from BLE
        var characteristicASCIIValue = NSString()
        guard characteristic == notifyChar,
        let characteristicValue = characteristic.value,
        let ASCIIstring = NSString(data: characteristicValue, encoding: String.Encoding.utf8.rawValue) else { return }
        characteristicASCIIValue = ASCIIstring
        let value = characteristicASCIIValue as String
        
        // handle output parsing
        handleReceivedValue(value: value)
    }
    
    private func handleReceivedValue(value: String){
        // check for empty response lines
        let replaced = cleanOutput(data: value)
        if( replaced.count > 0) {
            
            // optionally log received raw values also
            if( replaced != ">" ){
                log(message: "RX: \(replaced)")
            }
            
            // for regex
            let range = NSRange(location: 0, length: replaced.utf16.count)
            
            // collect data
            if( replaced.starts(with: "200") ){
                resetCollection(value: replaced)
            } else if( replaced.starts(with: "201") ){
                resetCollection(value: replaced)
            } else if (regex.firstMatch(in: replaced, options: [], range: range) != nil){
                appendCollection(value: replaced) // append valid hex responses
            }
            if( collected.count == 19 ){ //27 w/o /r> or 30 with or 19 w/o formatting
                print("parsed: \(collected)") // stop appending when no longer valid
                log(message: "RX: \(collected)")
                parseResponse(value: collected)
            }
        }
    }
    
    private func resetCollection(value: String){
        collected = value // collect
    }
    
    private func appendCollection(value: String){
        collected = "\(collected)\(value)"
    }
    
    private func parseResponse(value: String){
        collected = "" // prevents duplicated data parsing
        // proper parsing
        if( value.starts(with: "200") ){
            // note that either formatting with whitespace is enabled or not changing indeces
            tempMin = rangeAsInt(value: value, start: 3, end: 4)
            soc = rangeAsInt(value: value, start: 5, end: 6)
            soh = rangeAsInt(value: value, start: 7, end: 8)
            tempMax = rangeAsInt(value: value, start: 9, end: 10)
            let voltage10 = rangeAsSignedDouble(value: value, start: 11, end: 14)
            voltage = voltage10/10
            let amps10 = rangeAsSignedDouble(value: value, start: 15, end: 18)
            amps = amps10/10
            // parse charge power
            //let power1 = rangeAsInt(value: value, start: 15, end: 16)
            //let power2 = rangeAsInt(value: value, start: 17, end: 18)
            
            response = "\(response)\nSOC: \(soc) % (SOH: \(soh) %)\nTemp(min/max): \(tempMin)/\(tempMax) °C\nVoltage(bPackV): \(voltage) V\nAmps(bPackI): \(amps) A"
            
            power = voltage * 0.94 * amps / 1000
            // TODO calculate power from rpm instead like in BLE connection
            response = "\(response)\nPower(V*I): \(String(format: "%.1f", power)) kW"
            
            // charge power formula depends on charge state, no exact match
            // FIXME still needed now that we have the actual value? check not working? Always show both
            // let powerAC = power2/6
            // response = "\(response)\nPower(AC): \(powerAC) A"
            // let powerDC = 25*power1+power2/10
            // response = "\(response)\nPower(DC): \(powerDC) A"
            // response = "\(response)\nCharge Power(AC/DC): \(powerAC)/\(powerDC) A"
        }
        else if( value.starts(with: "201") ){
            // 201  01  00  00  00  00  00  00  00  // first digit is charge state, 01=IDLE, 02=AC, 10 or 16=DC charging
            // note that either formatting with whitespace is enabled or not changing indeces
            state = rangeAsInt(value: value, start: 3, end: 4)
            response = "\(response)\nState: \(state) (01=IDLE, 02=AC, 16=DC)"
        }
        else if (value.starts(with: "203")) {
            // 203 0F 40 43 2E 0F 36 0F 4D
            // first pair is also a cell voltage? Maybe average?
            // second pair no clue for example 67 & 46 or 17198 as a pair
            // last 2 pairs are cell balance in mV
            // note that either formatting with whitespace is enabled or not changing indeces
            cellMin = rangeAsInt(value: value, start: 11, end: 14)
            cellMax = rangeAsInt(value: value, start: 15, end: 18)
            response = "\(response)\nCell Balance: \(cellMin)/\(cellMax) mV"
        }
        
        if( value.starts(with: lastAtCommand) ){
            // update on screen value here to prevent flickering
            responseValue = response
        }
    }
    
    private func rangeAsInt(value: String, start: Int, end: Int) -> Int {
        let start = value.index(value.startIndex, offsetBy: start)
        let end = value.index(value.startIndex, offsetBy: end)
        let substring = String(value[start...end]).replacingOccurrences(of: " ", with: "")
        return Int(substring, radix: 16) ?? 0
    }
    
    private func rangeAsSignedDouble(value: String, start: Int, end: Int) -> Double {
        let start = value.index(value.startIndex, offsetBy: start)
        let end = value.index(value.startIndex, offsetBy: end)
        let substring = String(value[start...end]).replacingOccurrences(of: " ", with: "")
        return Double(Int16(bitPattern: UInt16(substring, radix: 16) ?? 0))
    }
    
    private func writeOutgoingValue(data: String, logOutput: Bool = true){
        if( logOutput ){
            let replaced = cleanOutput(data: data)
            if( replaced.count > 0) {
                log(message: "TX: \(replaced)")
            }
        }
        #if targetEnvironment(simulator)
            // nothing to push data to, just logging is fine for reference
        #else
            let valueString = (data as NSString).data(using: String.Encoding.utf8.rawValue)
            if let peripheral = self.peripheral {
                if let writeChar = self.writeChar {
                    peripheral.writeValue(valueString!, for: writeChar, type: CBCharacteristicWriteType.withResponse)
                  }
              }
        #endif
      }
    
    private func cleanOutput(data: String) -> String {
        let trimmed = data.trimmingCharacters(in: .whitespacesAndNewlines)
        // replacing all at once didn't give expected output
        //let replaced = trimmed.replacingOccurrences(of: " \r\r>", with: "")
        var replaced = trimmed.replacingOccurrences(of: " ", with: "")
        replaced = replaced.replacingOccurrences(of: "\r", with: "")
        replaced = replaced.replacingOccurrences(of: ">", with: "")
        return replaced
    }
    
}

#if DEBUG
struct Obd2View_Previews: PreviewProvider {
    static var previews: some View {
        Obd2ConnectView().environmentObject(BleControl())
    }
}
#endif
