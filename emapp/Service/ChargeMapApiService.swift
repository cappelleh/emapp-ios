//
//  ChargeMapApiService.swift
//  emapp
//
//  Created by Hans Cappelle on 27/04/2022.
//

import SwiftyJSON
import AFNetworking

class ChargeMapApiService {
    
    // see doc https://openchargemap.io/site/develop/api

    // reference info
    // https://api.openchargemap.io/v3/referencedata/?key=53b37716-5a32-4063-a5c6-f58447e19dce

    // example request (Kortrijk, BE) for DC (type2) chargers only within 20 km, to be checked every 10 km. Max 10 results
    // https://api.openchargemap.io/v3/poi?key=53b37716-5a32-4063-a5c6-f58447e19dce&latitude=50.802784&longitude=3.2095745&output=json&maxresults=10&distance=20&distanceUnit=KM&compact=true&connectiontypeid=33

    
    private let manager = AFHTTPSessionManager() // for network connection
    private let endpoint = "https://api.openchargemap.io/v3/poi"
    
    init() {
        manager.requestSerializer = AFJSONRequestSerializer()
        manager.responseSerializer = AFJSONResponseSerializer()
    }
    
    func retrieveChargers(
        latitude: Double,
        longitude: Double,
        success: @escaping (JSON) -> (),
        failure: @escaping (Error?) -> ()) {
        
        let params = [
            "key": "53b37716-5a32-4063-a5c6-f58447e19dce",
            "latitude": "\(latitude)",
            "longitude":"\(longitude)",
            "output":"json",
            "maxresults": "20",
            "distance": "20",
            "distanceUnit": "KM",
            "compact": "true"//,
            //"connectiontypeid": "33" // CCS chargers FIXME need a different type for US
        ]
        manager.get(endpoint,
                    parameters: params,
                    headers: [:],
                    progress: { (progress: Progress) -> Void in
            // ignored
        },
                    success:{
                        (task: URLSessionDataTask!, responseObject: Any?) in
                        print("success, got a chargemap response")
                        success(JSON(responseObject))
                        
        },
                    failure: {
                        (task: URLSessionDataTask!, error: Error?) in
                        print("Error: \(error!.localizedDescription)")
                        failure(error)
        })
    }
    
}
