# About 

Generic App to get information from electric motorcycles using either Bluetooth or OBDII connection. 
Optionally this gathered info can also be shared using cloud services. 

Android Play Store link https://play.google.com/store/apps/details?id=be.hcpl.android.energica

iOS App Store link https://apps.apple.com/app/id1559468042

Zero specific Android App https://play.google.com/store/apps/details?id=be.hcpl.android.zengo

Zero specific iOS App https://apps.apple.com/us/app/zerong/id1488172044

# Upcoming & Feature requests

* full ev-monitor.com data upload integration
* move OBDII data fetching to backgroud task
* implement BLE connection with Energica directly
* for charger add route option on map
* show route trail on map from gps location
* fix reset of map center on each position update

# Version History

## 2.15

* added title to chargers on map
* show charger name and address 

## 2.14

* fixed changing zoom level on map

## 2.13

* show nearby chargers on map

## 2.12

* fixed layout updates on receiving new OBDII values

## 2.11

* small layout improvements
* update map based on current location
* clean up of some unused features
* fixed broken cell balance command

## 2.10

* improvements on output logging
* fixed OBD sequence

## 2.9

* updates on map view showing current location (wip)

## 2.8

* fixed temporary black out of data while parsing
* allow for custom interval for monitor
* print more decimal converted values, incl 2 complement

## 2.7

* fixed unit on charge power (A not kW)
* display both bPackV and bPackI values
* put SOH value between brackets, even dead battery is still reporting SOH 100%
* display calculated power value (V*I)
* more data layout improvements

## 2.6

* bugfix for can bus scanning can id format
* moved scanning interval to hidden section
* more layout improvements
* changed monitor interval from 4 to 6 seconds

## 2.5

* more layout improvements
* on device logs kept in app sandbox
* hidden decimal and raw output feature
* add toggle for logs on screen

## 2.4

* more log output improvements
* include cell voltage from parsing 203 response
* faster updates on monitoring

## 2.3

* added auto updating feature (monitoring)
* allow for custom VCU requests
* fixed response legnth check

## 2.2

* changed layout dropping contact tab (blog has contact info)
* added initial GPS tracking tab with Map view
* clear log and response buttons added
* show more values in app for debugging

## 2.1

* added charge state and charge power values from OBDII
* improvements on BLE state handling

## 2.0

Major update with initial BLE elm327 OBDII dongle integration for parsing Energica specific values  

* OBD2 ELM327 integration for Energica

## 1.7

* proper fix for off screen cookies consent for iPhone 11 and later by Mr. Joris

## 1.6

* Fixed bug where cookies consent screen on web content wasn't visible to user
* Fixed this missing version history and readme file in this repo

## 1.5

* moved iOS app to separate repo
* update app version, apply energica specific penalty to improve upon estimated range

## 1.4

More charge time calculation improvements

* completed charge time calculation on nominal capacity including formatting
* use nominal capacity for charge time calculation
* display nominal capacity for batteries, add 24kW charge option

## 1.3

Charge time calculation improvements

* update charge time to calculate with 85% of total capacity
* added charge time calculation in minutes

## 1.2

* fix app icon

## 1.1

Improvements and bug fixes

* temp icon provided
* fixed smaller iphone 8 screen support
* completed range calculator
* progress implementing range calculator tool
* progress implementing range calculator tool
* added reloading for video view

## 1.0

Original release of this app
 
* Project setup for iOS including simple app having tabs with range calculator (incomplete) and webviews for reference and video

# Getting Started with Development

Required steps:

* install xcode and cocoapods
* clone project
* fetch dependencies
* open project in xcode

Xcode can be installed from App Store or for a specific version from the downloads section in your development account.

For cocoapods installation I used below command. You might have to install homebrew first or use the alternative command 
from cocoapods website (see below).

Specific to running pods on Apple M1 chip I had to use this workaround https://stackoverflow.com/questions/64901180/running-cocoapods-on-apple-silicon-m1

```
brew install cocoapods
```

This project uses Pods to manage dependencies. So after checkout (using git clone) navigate into project folder 
and update dependencies using below command. Only then a project file will be created that you can open with XCode.

```
pod install
```

# References

## Android implementation 

Originally created for Android https://bitbucket.org/cappelleh/energica-bluetooth-android/src/master/
That code will also be merged into Android app available at https://bitbucket.org/cappelleh/emapp-android/src/master/

## App icons & other assets

For icons in app I used system images, for more info see https://sfsymbols.com/

App screenshots with panorama background generated at https://studio.app-mockup.com/

## SwiftUI

In my desire to stay away from storyboards I implemented this app as a SwiftUI app. Not realising there isn't that much code around yet at this point. So most of the references here are pointing to code examples using UIViewControllers. When I realised this mistake it was already way too late to turn back. So here are a few references specific to SwiftUI. 

What I've probably should've done was https://medium.com/@Dougly/a-uiviewcontroller-and-uiviews-without-storyboard-swift-3-543096e78f2 or this https://betterprogramming.pub/how-to-build-your-user-interface-programatically-without-swiftui-9f0dc52e02bc which are all other ways to create Swift Apps without storyboards. 

You create an `App` and a `Scene` and `Views`. 

```
import SwiftUI

@main
struct EmappApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
```

Those `Views` have no `UIViewController` for the logic. Instead they can be linked to an `ObservableObject` which in turn can have `@Published` elements that the `View` code can refer to. That linked is something you can't forget, for ref see https://www.hackingwithswift.com/quick-start/swiftui/how-to-use-environmentobject-to-share-data-between-views

```
    @StateObject var bleControl = BleControl()
    
    var body: some View {
        // tabs, for some tabs force refresh on loading
        TabView(selection: /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Selection@*/.constant(1)/*@END_MENU_TOKEN@*/) {
            RangeView().tabItem { Label("Range", systemImage:"scribble") }.tag(1)
            Obd2ConnectView().tabItem { Label("OBDII", systemImage:"bolt.car") }.tag(2).environmentObject(bleControl)
            
    }
```

Another resource for these `ObservableObject` and how to push changes from there to the view is https://www.hackingwithswift.com/books/ios-swiftui/manually-publishing-observableobject-changes

And this is good info on how to manage `Views` and properties themselves, see https://cocoacasts.com/swiftui-fundamentals-customizing-views-with-modifiers

Now to get the BLE info I found working within such an `ObservableObject` I had to combine info from these posts https://developer.apple.com/forums/thread/123568 and https://stackoverflow.com/questions/58239721/render-list-after-bluetooth-scanning-starts-swiftui

How we check in View on the connection

```
struct Obd2ConnectView: View {
    
    @EnvironmentObject var bluetooth: BleControl // reference to our BLE connection
     
    var body: some View {
        VStack {
            if( bluetooth.connected ){
                //Text(verbatim: String(decodeValue(data: bluetooth.characteristicValue).rpm))
                Text("OBDII Connection is LIVE")
                // shows connected but also reveal options to get data from OBDII port and log info
            } else {
                Text("...")
            }
        }
    }
```

The BLE connection is handled here
```
class BleControl: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate, ObservableObject {
    
    @Published var connected = false
    @Published var characteristicValue = ""

    static let BLESingleton = BleControl() // implemented as a singleton
    override init() {}

    // needed for BLE connection
    private var centralManager: CBCentralManager!
    private var peripheral: CBPeripheral!
     
     func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        self.connected = true
        print("connected")
        peripheral.discoverServices(nil)
     }    
}
```

## OBD2 integration

iOS libraries, 2 options found so far (I'll update as I made a choice)

https://github.com/Wisors/OBD2Connect // swift but wifi based, not bluetooth

https://github.com/mickeyl/LTSupportAutomotive // no swift library

Making a BLE connection on iOS, great tutorial at https://www.freecodecamp.org/news/ultimate-how-to-bluetooth-swift-with-hardware-in-20-minutes/

UUID that I need for this are UART specific, from https://stackoverflow.com/questions/61090365/what-ble-characteristic-should-i-use-in-a-ble-ios-device-for-obdii

```
Service Service: UUID: 0000fff0-0000-1000-8000-00805f9b34fb
    use the notify of this one...
Charac.: UUID: 0000fff1-0000-1000-8000-00805f9b34fb
    Can read: false Can write: false  Can notify: true
Charac.: UUID: 0000fff2-0000-1000-8000-00805f9b34fb
    use the write of this one...
    Can read: false Can write: true  Can notify: false
```    
    
Another good example with more info on how to read and write https://learn.adafruit.com/build-a-bluetooth-app-using-swift-5?view=all

## Running background tasks on iOS

I'll have to make this work for location updates (GPS route tracking) but also to get the OBDII updates in the background and pushing results to https://ev-monitor.com. 

For more info and a tutorial on how to do so visit https://www.raywenderlich.com/5817-background-modes-tutorial-getting-started from https://developer.apple.com/forums/thread/119738 and also https://developer.apple.com/documentation/backgroundtasks

## Mapview in SwiftUI

See https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-a-map-view
And how to use LocationManager in combination with Swift UI https://www.andyibanez.com/posts/using-corelocation-with-swiftui/

How to show markers on map https://www.hackingwithswift.com/quick-start/swiftui/how-to-show-annotations-in-a-map-view
Manually register for updates https://stackoverflow.com/questions/65554158/subscribe-swiftui-view-to-every-update-of-a-combine-publisher

And instructions on how to create map annotations https://kristaps.me/blog/swiftui-map-annotations/

# Contact details

See repo owners. I'm also on YouTube as https://www.youtube.com/channel/UCMB-LNAuNJTAsFZgakBkpxQ and I have this blog at https://fotoleer.wordpress.com/electric-motorcycles/  
